#ifndef DONJON_H
#define DONJON_H

#define MONSTRE_POSSIBLE_DANS_UN_DONJON NOMBRE_DE_MONSTRES

#include "AVENTURIER.h"
#include "MONSTRE.h"

// Type donjon
typedef struct      S_Donjon
{
        Monstre*    cartes_monstres[TAILLE_MAX_NOM_DU_MONSTRE];
}                   Donjon;

void                initialiser_le_donjon(Donjon* donjon);
void                ajouter_un_monstre_au_donjon(Donjon* donjon, Monstre* monstre);

#endif // DONJON_H
