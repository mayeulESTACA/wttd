BINNAME  = WttD

COMPILER = gcc

CFLAGS   = -O3
CFLAGS  += -W
CFLAGS  += -Wall
CFLAGS	+= -Wextra
CFLAGS	+= -Werror

DIR_SRC	 = sources
DIR_INC  = includes

RM	 = rm -f

SOURCES	 = $(shell find $(DIR_SRC) -type f -name "*.c")

all:	$(BINNAME)


$(BINNAME):	$(SOURCES)
	$(COMPILER) -I $(DIR_INC) $(CFLAGS) $(SOURCES) -o $(BINNAME) 

clean:
	$(RM) $(BINNAME)

re:	clean all

.PHONY:	all clean
