#ifndef MISE_EN_PLACE_H
#define MISE_EN_PLACE_H

#include "AVENTURIER.h"
#include "JOUEUR.h"

unsigned int	lire_un_chiffre_depuis_la_console(const char* message_de_demande, const char* message_invalide, unsigned int valeur_minimale, unsigned int valeur_maximale);
void            demander_le_pseudo_de_chaque_joueurs(unsigned int nombre_de_joueurs, Joueur* joueurs);
Aventurier*     choisir_un_aventurier_pour_aller_au_donjon(Aventurier* aventuriers);

#endif // MISE_EN_PLACE_H
