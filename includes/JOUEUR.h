#ifndef JOUEUR_H
#define JOUEUR_H

#include "MONSTRE.h"
#include "PIOCHE_MONSTRE.h"

#define TAILLE_MAX_NOM_DU_JOUEUR 32
#define JOUEUR_A_PASSER          1
#define JOUEUR_JOUE_ENCORE       2
#define NOMBRE_DE_JOUEURS_MAX    4
#define NOMBRE_DE_JOUEURS_MIN    2

typedef struct      s_Joueur
{
    char            pseudo[TAILLE_MAX_NOM_DU_JOUEUR];
    unsigned int    id;                                  //Num�ro du joueur
    unsigned int    nombre_de_victoires_du_donjon;
    unsigned int    passer_le_tour;
    Monstre*        monstre_dans_ma_main;
}                   Joueur;

void                joueur_tire_une_carte_monstre(Joueur* moi, PiocheMonstre* pioche);

#endif // JOUEUR_H
