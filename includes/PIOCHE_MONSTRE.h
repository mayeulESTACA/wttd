#ifndef PIOCHE_MONSTRE_H
#define PIOCHE_MONSTRE_H

#include "MONSTRE.h"

//--Pioche monstre

typedef struct      S_PiocheMonstre
{
        Monstre* cartes_monstres[13];
}                   PiocheMonstre;

void                initialiser_la_pioche(PiocheMonstre* pioche_monstre, Monstre* liste_monstre);
Monstre*            piocher_un_monstre(PiocheMonstre* pioche_monstre);

#endif // PIOCHE_MONSTRE_H
