#include "JOUEUR.h"
#include "PIOCHE_MONSTRE.h"

void joueur_tire_une_carte_monstre(Joueur* moi, PiocheMonstre* pioche_monstre)
{
        moi->monstre_dans_ma_main = piocher_un_monstre(pioche_monstre);
}
