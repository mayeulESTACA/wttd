#include <stdio.h>
#include <stdlib.h>

#include "DONJON.h"

void                initialiser_le_donjon(Donjon* donjon)
{
    for (unsigned int index_monstre_donjon = 0; index_monstre_donjon <= MONSTRE_POSSIBLE_DANS_UN_DONJON; index_monstre_donjon++)
        donjon->cartes_monstres[index_monstre_donjon] = NULL;
}

void                ajouter_un_monstre_au_donjon(Donjon* donjon, Monstre* monstre)
{
    unsigned int    index_monstre_donjon = 0;

    for (index_monstre_donjon = 0; donjon->cartes_monstres[index_monstre_donjon] != NULL; index_monstre_donjon++);
    donjon->cartes_monstres[index_monstre_donjon] = monstre;
}
