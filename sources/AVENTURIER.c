#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "EFFETS.h"
#include "EQUIPEMENTS.h"
#include "AVENTURIER.h"

void initialiser_les_aventuriers(Aventurier* aventuriers, Effet* effets, Equipement* equipements)
{
    strcpy(aventuriers[0].nom,"Voleuse");    aventuriers[0].PV=3;   aventuriers[0].equipements[0] = &(equipements[0]); aventuriers[0].equipements[1] = &(equipements[1]); aventuriers[0].effets[0] = &(effets[0]); aventuriers[0].effets[1] = &(effets[1]);  aventuriers[0].effets[2] = &(effets[2]);  aventuriers[0].effets[3] = &(effets[12]);
    strcpy(aventuriers[1].nom,"Barbare");    aventuriers[1].PV=4;   aventuriers[1].equipements[0] = &(equipements[2]); aventuriers[1].equipements[1] = &(equipements[3]); aventuriers[1].effets[0] = &(effets[3]); aventuriers[1].effets[1] = &(effets[4]);  aventuriers[1].effets[2] = &(effets[5]);  aventuriers[1].effets[3] = &(effets[12]);
    strcpy(aventuriers[2].nom,"Mage");       aventuriers[2].PV=2;   aventuriers[2].equipements[0] = &(equipements[4]); aventuriers[2].equipements[1] = &(equipements[5]); aventuriers[2].effets[0] = &(effets[6]); aventuriers[2].effets[1] = &(effets[7]);  aventuriers[2].effets[2] = &(effets[8]);  aventuriers[2].effets[3] = &(effets[9]);
    strcpy(aventuriers[3].nom,"Guerrier");   aventuriers[3].PV=3;   aventuriers[3].equipements[0] = &(equipements[6]); aventuriers[3].equipements[1] = &(equipements[7]); aventuriers[3].effets[0] = &(effets[5]); aventuriers[3].effets[1] = &(effets[10]); aventuriers[3].effets[2] = &(effets[11]); aventuriers[3].effets[3] = &(effets[9]);
}

void afficher_les_equipements_de_aventurier (Aventurier* aventurier)
{
    printf("L'aventurier est %s et ses �quipements sont :\n", aventurier->nom);
    afficher_les_equipements(aventurier->equipements);
    afficher_les_effets(aventurier->effets);
    printf("\n\n");
}

void retirer_un_equipement_de_aventurier(Aventurier* aventurier, unsigned int choix_equipement_a_enlever)
{
    if (choix_equipement_a_enlever < NOMBRE_MAX_EQUIPEMENTS)
    {
        printf("L'�quipement %s a �t� enlev�\n", aventurier->equipements[choix_equipement_a_enlever]->nom);
        aventurier->equipements[choix_equipement_a_enlever] = NULL;
    }
    else if (choix_equipement_a_enlever < NOMBRE_MAX_EQUIPEMENTS + NOMBRE_MAX_EFFETS)
    {
        printf("L'effet %s a �t� enlev�\n", aventurier->effets[choix_equipement_a_enlever - NOMBRE_MAX_EQUIPEMENTS]->nom);
        aventurier->effets[choix_equipement_a_enlever - NOMBRE_MAX_EQUIPEMENTS] = NULL;
    }
    else
        printf("L'aventurier ne poss�de pas cet �quipement\n");
}
