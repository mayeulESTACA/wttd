#ifndef EQUIPEMENTS_H
#define EQUIPEMENTS_H

#define NOMBRE_DES_EQUIPEMENTS          13
#define TAILLE_MAX_NOM_DES_EQUIPEMENTS  32

typedef struct  S_Equipement
{
    char        nom[TAILLE_MAX_NOM_DES_EQUIPEMENTS];
    int         PV;
}               Equipement;

void            initialiser_les_equipements(Equipement* equipements);
void            afficher_les_equipements(Equipement** equipements);

#endif // EQUIPEMENTS_H
