#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "MISE_EN_PLACE.h"
#include "JOUEUR.h"
#include "AVENTURIER.h"

unsigned int	        lire_un_chiffre_depuis_la_console(const char* message_de_demande, const char* message_invalide, unsigned int valeur_minimale, unsigned int valeur_maximale)
{
  char		reponse[2]  = "";
  unsigned int	chiffre = 0;

  while (42)
    {
        sleep(1);
        write(1, message_de_demande, strlen(message_de_demande));
        reponse[0] = '\0';
        reponse[1] = '\0';
        if (read(1, reponse, 2) == 2)
        {
            if ((reponse[0] != '\n') && (reponse[1] == '\n'))
            {
                if ((reponse[0] >= '0') && (reponse[0] <= '9'))
                {
                    chiffre = reponse[0] - '0';
                    if ((chiffre >= valeur_minimale) && (chiffre <= valeur_maximale))
                        return (chiffre);
                }
                write(1, message_invalide, strlen(message_invalide));
            }
            else
            {
                while ((read(1, reponse, 1) == 1) && (reponse[0] != '\n'));
                write(1, message_invalide, strlen(message_invalide));
            }
        }
    }
}

void                demander_le_pseudo_de_chaque_joueurs(unsigned int nombre_de_joueurs, Joueur* joueurs)
{
    unsigned int    joueur_id = 0;
    char            joueur_pseudo[TAILLE_MAX_NOM_DU_JOUEUR] = "";

    if (joueurs != NULL)
    {
        while (joueur_id != nombre_de_joueurs)
        {
            printf("\n----Joueur %d : entrez votre pseudo: ", joueur_id + 1);
            scanf("%s", joueur_pseudo);
            joueurs[joueur_id].id = joueur_id;
            joueurs[joueur_id].passer_le_tour = JOUEUR_JOUE_ENCORE;
            strcpy(joueurs[joueur_id].pseudo, joueur_pseudo);
            joueur_id++;
        }
        printf("\n");
    }
    else
        printf("ERREUR: demander_le_pseudo_de_chaque_joueurs: joueurs est NULL\n");
}

Aventurier*         choisir_un_aventurier_pour_aller_au_donjon(Aventurier* aventuriers)
{
    unsigned int    choix_aventurier = 0;

    printf(" ___________  Choississez votre aventurier  ______________________________________ \n");
    printf("|                                                                                 |\n");
    printf("|   1. Jouer avec la voleuse                                                      |\n");
    printf("|   2. Jouer avec le barbare                                                      |\n");
    printf("|   3. Jouer avec le mage                                                         |\n");
    printf("|   4. Jouer avec le guerrier                                                     |\n");
    printf("|_________________________________________________________________________________|\n");
    choix_aventurier = lire_un_chiffre_depuis_la_console("Choisissez votre aventurier: ", "Le choix n'est pas valide\n", 1, 4);
    printf("\n");
    return (&(aventuriers[choix_aventurier - 1]));
}
