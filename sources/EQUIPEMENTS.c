#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "EQUIPEMENTS.h"
#include "AVENTURIER.h"

void initialiser_les_equipements(Equipement* equipements)
 {
    strcpy(equipements[0].nom, "Rondache");                  equipements[0].PV = 3;
    strcpy(equipements[1].nom, "Armure de Mitril");          equipements[1].PV = 5;
    strcpy(equipements[2].nom, "Bouclier en cuir");          equipements[2].PV = 4;
    strcpy(equipements[3].nom, "Cotes de mailles");          equipements[3].PV = 3;
    strcpy(equipements[4].nom, "Bracelet de protection");    equipements[4].PV = 3;
    strcpy(equipements[5].nom, "Mur de feu");                equipements[5].PV = 6;
    strcpy(equipements[6].nom, "Bouclier de chevalier");     equipements[6].PV = 3;
    strcpy(equipements[7].nom, "Armure de plates");          equipements[7].PV = 5;
 }

void   afficher_les_equipements(Equipement** equipements)
{
    for (unsigned int index_equipement = 0; index_equipement < NOMBRE_MAX_EQUIPEMENTS; index_equipement++)
    {
        if (equipements[index_equipement] != NULL)
            printf("|--%d:-%-25s-------PV=%d------------------------------------------------------------------------------|\n", index_equipement + 1, equipements[index_equipement]->nom, equipements[index_equipement]->PV);
    }
}
