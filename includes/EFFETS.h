#ifndef EFFETS_H
#define EFFETS_H

#define NOMBRE_D_EFFETS                      13
#define TAILLE_MAX_NOM_DE_L_EFFET            32
#define TAILLE_MAX_DESCRIPTION_DE_L_EFFET   128

// Type effets
typedef struct      S_Effet
{
        char        nom[TAILLE_MAX_NOM_DE_L_EFFET];
        char        description[TAILLE_MAX_DESCRIPTION_DE_L_EFFET];
}                   Effet;

void                initialiser_les_effets(Effet* effets);
void                afficher_les_effets(Effet** effets);

#endif // EFFETS_H
