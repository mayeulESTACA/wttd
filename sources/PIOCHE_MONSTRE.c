#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "PIOCHE_MONSTRE.h"


//   Tableau des cartes monstres
void                initialiser_la_pioche(PiocheMonstre* pioche_monstre, Monstre* liste_monstre)
 {
    pioche_monstre->cartes_monstres[0]  = &(liste_monstre[0]);
    pioche_monstre->cartes_monstres[1]  = &(liste_monstre[1]);
    pioche_monstre->cartes_monstres[2]  = &(liste_monstre[2]);
    pioche_monstre->cartes_monstres[3]  = &(liste_monstre[3]);
    pioche_monstre->cartes_monstres[4]  = &(liste_monstre[4]);
    pioche_monstre->cartes_monstres[5]  = &(liste_monstre[5]);
    pioche_monstre->cartes_monstres[6]  = &(liste_monstre[6]);
    pioche_monstre->cartes_monstres[7]  = &(liste_monstre[7]);
    pioche_monstre->cartes_monstres[8]  = &(liste_monstre[8]);
    pioche_monstre->cartes_monstres[9]  = &(liste_monstre[9]);
    pioche_monstre->cartes_monstres[10] = &(liste_monstre[10]);
    pioche_monstre->cartes_monstres[11] = &(liste_monstre[11]);
    pioche_monstre->cartes_monstres[12] = &(liste_monstre[12]);
 }

 Monstre*           piocher_un_monstre(PiocheMonstre* pioche_monstre)
 {
     Monstre*       monstre = NULL;
     int            monstre_pioche = 0;
     unsigned int   compteur_de_cartes = 0;

    srand(time(NULL));
     for (unsigned int index_carte = 0; index_carte != NOMBRE_DE_MONSTRES; index_carte++)
     {
         if (pioche_monstre->cartes_monstres[index_carte] != NULL)
            compteur_de_cartes++;
     }
     if (compteur_de_cartes == 0)
     {
         printf("ERREUR: La pioche de mostres est vide :/\n");
         return (NULL);
     }
     while (42)
     {
        monstre_pioche = rand() % NOMBRE_DE_MONSTRES;
        if (pioche_monstre->cartes_monstres[monstre_pioche] != NULL)
        {
            monstre = pioche_monstre->cartes_monstres[monstre_pioche];
            pioche_monstre->cartes_monstres[monstre_pioche] = NULL;
            afficher_la_carte_monstre(monstre);
            return (monstre);
        }
     }
     return (NULL);
 }


