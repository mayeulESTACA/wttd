#ifndef AVENTURIER_H
#define AVENTURIER_H

#define NOMBRE_D_AVENTURIERS            4
#define TAILLE_MAX_NOM_DES_AVENTURIERS  32
#define NOMBRE_MAX_EQUIPEMENTS          2
#define NOMBRE_MAX_EFFETS               4

#include "EFFETS.h"
#include "EQUIPEMENTS.h"

// Type aventurier
typedef struct      S_Aventurier
{
        char        nom[TAILLE_MAX_NOM_DES_AVENTURIERS];
        int         PV;
        Effet*      effets[NOMBRE_MAX_EFFETS];
        Equipement* equipements[NOMBRE_MAX_EQUIPEMENTS];
}                   Aventurier;

void                initialiser_les_aventuriers(Aventurier* aventuriers, Effet* effets, Equipement* equipements);
void                afficher_les_equipements_de_aventurier (Aventurier* aventurier);
void                retirer_un_equipement_de_aventurier(Aventurier* aventurier, unsigned int choix_equipement_a_enlever);

#endif // AVENTURIER_H
