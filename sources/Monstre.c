#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "MONSTRE.h"

void            initialiser_les_monstres(Monstre* monstres)
{
    strcpy(monstres[0].nom,  "golem");              monstres[0].puissance  = 3;
    strcpy(monstres[1].nom,  "golem");              monstres[1].puissance  = 3;
    strcpy(monstres[2].nom,  "squelette");          monstres[2].puissance  = 2;
    strcpy(monstres[3].nom,  "squelette");          monstres[3].puissance  = 2;
    strcpy(monstres[4].nom,  "demon");              monstres[4].puissance  = 7;
    strcpy(monstres[5].nom,  "dragon");             monstres[5].puissance  = 9;
    strcpy(monstres[6].nom,  "sorcier");            monstres[6].puissance  = 6;
    strcpy(monstres[7].nom,  "monstre de pierre");  monstres[7].puissance  = 5;
    strcpy(monstres[8].nom,  "monstre de pierre");  monstres[8].puissance  = 5;
    strcpy(monstres[9].nom,  "vampire");            monstres[9].puissance  = 4;
    strcpy(monstres[10].nom, "vampire");            monstres[10].puissance = 4;
    strcpy(monstres[11].nom, "gobelin");            monstres[11].puissance = 1;
    strcpy(monstres[12].nom, "gobelin");            monstres[12].puissance = 1;
}

void            afficher_la_carte_monstre(Monstre* monstre)
{
    printf("La carte pioch�e est: \n");
    printf(" __________________________\n");
    printf("| Nom: %-20s|\n", monstre->nom);
    printf("|       puissance = %d      |\n", monstre->puissance);
    printf("|__________________________|\n\n");
}
