CODE_BLOCKS_PATH="${1}"
DIR_SOURCES="sources"
DIR_INCLUDES="includes"

if [ -d "${CODE_BLOCKS_PATH}" ]
then
    if [ -d ${DIR_SOURCES} ]
    then
	cp "${CODE_BLOCKS_PATH}"/*.c ${DIR_SOURCES}
	chmod 644 ${DIR_SOURCES}/*.c
	dos2unix  ${DIR_SOURCES}/*.c
    else
	echo "ERREUR: pas de dossier ${DIR_SOURCES} détecté"
    fi
    if [ -d ${DIR_INCLUDES} ]
    then
	cp "${CODE_BLOCKS_PATH}"/*.h ${DIR_INCLUDES}
	chmod 644 ${DIR_INCLUDES}/*.h
	dos2unix  ${DIR_INCLUDES}/*.h
    else
	echo "ERREUR: pas de dossier ${DIR_INCLUDES} détecté"
    fi
else
    echo "ERREUR: le chemin d'accès fourni en paramètre de ce script est soit incorrect, soit invalid, soit n'existe pas soit les privilèges d'accès sont insuffisants"
fi
