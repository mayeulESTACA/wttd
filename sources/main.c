#include <stdio.h>
#include <stdlib.h>

#include "JOUEUR.h"
#include "MISE_EN_PLACE.h"
#include "MONSTRE.h"
#include "EQUIPEMENTS.h"
#include "EFFETS.h"
#include "AVENTURIER.h"
#include "PIOCHE_MONSTRE.h"
#include "ENCHERES.h"

int main()
{
    Joueur           joueurs[NOMBRE_DE_JOUEURS_MAX];
    Monstre          monstres[NOMBRE_DE_MONSTRES];
    Equipement       equipements[NOMBRE_DES_EQUIPEMENTS];
    Effet            effets[NOMBRE_D_EFFETS];
    Aventurier       aventuriers[NOMBRE_D_AVENTURIERS];
    PiocheMonstre    pioche_monstre;
    Donjon           donjon;

    initialiser_les_monstres(monstres);
    initialiser_les_equipements(equipements);
    initialiser_les_effets(effets);
    initialiser_les_aventuriers(aventuriers, effets, equipements);
    initialiser_la_pioche(&pioche_monstre, monstres);
    initialiser_le_donjon(&donjon);

    unsigned int nombre_de_joueurs = lire_un_chiffre_depuis_la_console("Saisissez le nombre de joueurs: ", "Le nombre de joueurs n'est pas valide\n", NOMBRE_DE_JOUEURS_MIN, NOMBRE_DE_JOUEURS_MAX);
    demander_le_pseudo_de_chaque_joueurs(nombre_de_joueurs, joueurs);
    Aventurier*  choix_aventurier = choisir_un_aventurier_pour_aller_au_donjon(aventuriers);
    if (choix_aventurier == NULL)
    {
        printf("Le choix du nombre de joueurs n'est pas valide :-)\n");
        return (-1);
    }
    else
        printf("Vous avez sélectionné l'aventurier %s\n\n", choix_aventurier->nom);
    for (unsigned int joueur_choisi = 0; 1 == 1; joueur_choisi++)
    {
        if (joueur_choisi == nombre_de_joueurs)
            joueur_choisi = 0;
        if (joueurs[joueur_choisi].passer_le_tour == JOUEUR_JOUE_ENCORE)
        {
            printf("Je suis le joueur %s \n",joueurs[joueur_choisi].pseudo);
            lance_les_encheres(&joueurs[joueur_choisi], &donjon, &pioche_monstre, choix_aventurier);
        }
        else if (joueurs[joueur_choisi].passer_le_tour == JOUEUR_A_PASSER)
        {
            printf("%s, vous avez eu peur de continuer cette aventure.\nVous ne pouvez donc plus jouer lors de cette manche\n\n", joueurs[joueur_choisi].pseudo);
        }
    }
    return (0);
}
