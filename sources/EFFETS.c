#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "EFFETS.h"
#include "EQUIPEMENTS.h"
#include "AVENTURIER.h"

//   Tableau des cartes avec des effets
void initialiser_les_effets(Effet* effets)
 {
    strcpy(effets[0].nom,  "anneau de pouvoir");      strcpy(effets[0].description,  "ignorer les monstres de puissance 2 ou moins, ajoutez leur puissances � vos PV");
    strcpy(effets[1].nom,  "cape d'invisibilit�");    strcpy(effets[1].description,  "ignorer les monstres de puissance 6 ou plus");
    strcpy(effets[2].nom,  "dague vorpale");          strcpy(effets[2].description,  "ignorer un monstre que vous choisissez avant d'entrer dans le donjon ");
    strcpy(effets[3].nom,  "hache vorpale");          strcpy(effets[3].description,  "ignorer un monstre apr�s l'avoir pioch�");
    strcpy(effets[4].nom,  "marteau de guerre");      strcpy(effets[4].description,  "ignorer les golems");
    strcpy(effets[5].nom,  "torche");                 strcpy(effets[5].description,  "ignorer les monstres de puissance 3 ou moins");
    strcpy(effets[6].nom,  "pacte avec le demon");    strcpy(effets[6].description,  "ignorer le d�mon et le monstre suivant");
    strcpy(effets[7].nom,  "omnipotence");            strcpy(effets[7].description,  "si tous les monstres du donjon sont diff�rents vous remportez la manche");
    strcpy(effets[8].nom,  "polymorphe");             strcpy(effets[8].description,  "ignorer un monstre que vous piochez, remplacer le par le premier de la pioche");
    strcpy(effets[9].nom,  "saint graal");            strcpy(effets[9].description,  "ignorer les monstres de puissance paire");
    strcpy(effets[10].nom, "lance dragon");           strcpy(effets[10].description, "ignorer le dragon");
    strcpy(effets[11].nom, "�p�e vorpale");           strcpy(effets[11].description, "ignorer un monstre que vous choisissez avant d'entrer dans le donjon");
    strcpy(effets[12].nom, "potion de soin");         strcpy(effets[12].description, "ignorer les monstres de puissance 3 ou moins");
 }

void afficher_les_effets(Effet** effets)
{
    for (unsigned int index_effet = 0; index_effet < NOMBRE_MAX_EFFETS; index_effet++)
    {
        if (effets[index_effet] != NULL)
            printf("|--%d:-%-25s--------%-80s--|\n", index_effet + NOMBRE_MAX_EQUIPEMENTS + 1, effets[index_effet]->nom, effets[index_effet]->description);
    }
}
