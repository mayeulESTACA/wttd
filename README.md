# Jeu console "Welcome to the Dungeon" #
Ce dépôt a été créé dans le but de review le code par des dévelopeurs professionnels écrit par Mayeul et Arthur pour le projet WttD dans le cadre de leurs études pour l'ESTACA.


### Prérequis ###
- Avoir WSL activé
    - Rechercher dans le menu démarrer "Activer ou désactiver des fonctionnalités de Windows"
    - Cocher la case `Sous-système Windows pour Linux` (vers le bas)
    - Redémarrer Windows
- Avoir Ubuntu installé
    - Ouvrir le Microsoft Store
    - Rechercher Ubuntu
    - Installer `Ubuntu` ou `Ubuntu 20.04 LTS`
- Avoir Ubuntu configuré
    - Lancer WSL (rechercher dans le menu démarrer)
    - Ajouter WSL à la barre des tâches
    - Ajouter un nom d'utilisateur UNIX (par exemple votre prénom)
    - Définir un mot de passe pour cet utilisateur
    - Mettre à jour le système : `sudo apt-get update && sudo apt-get upgrade -y`
    - Installer les programmes necéssaires `sudo apt-get install vim nano emacs htop gcc valgrind tree make git curl wget unzip zip tar dos2unix -y`
- Avoir WSL d'ouvert
    - Ouvrir WSL (menu démarrer ou barre de tâches)


### Installation et utilisation ###
- Allez dans votre home: `cd ~`
- Clonez le projet sur votre machine `git clone git@bitbucket.org:dupoug_h/wttd.git wttd`
- Allez dans le dossier que vous venez de télécharger `cd wttd`


### Pour y jouer ###
- Compilez le projet `make`
- Jouez en lançant le jeu comme suit: `./WttD`


### Pour développer ###
Le projet comporte deux dossiers:
`sources`  pour les sources (fichiers en \*c)
`includes`  pour les headers (fichiers en \*.h)

Aller dans le dossier GIT:
`cd ~/wttd`

La compilation se fait avec le script suivant: `./compile.sh`

- La compilation `DEBUG` se fera avec `gcc -g3 -Wextra -Wall -Werror *.c`
- La compilation `NORMAL` se fera avec `gcc -O3 -Wextra -Wall -Werror *.c`
- L'exécution `DEBUG` se fera avec valgrind: `valgrind ./WttD`
- L'exécution `NORMAL` se fera sans valgrind: `./WttD`

Ce système permet de tester et d'avoir un programme avec le minimum d'erreurs possibles


### Contributions ###
- Seuls Mayeul ou Arthur peuvent directement apporter des modifications à leur propre code
- Toute personne extérieur ou étudiant de l'ESTACA est libre de soumettre sa pull request (PR) pour review

- Procédure (à faire une fois):
    - Vérifiez que vous ayez un compte BitBucket (https://id.atlassian.com/signup?application=bitbucket)
    - Connectez-vous sur BitBucket (https://bitbucket.org/dashboard/overview)
    - Retournez sur votre console WSL
    - Allez dans votre home: `cd ~/WttD`
    - Configurez git: `git config --global user.email "monemailducomptebitbucket@untruc.lol"`
    - Configurez git: `git config --global user.name  "Mon prénom ET MON NOM"`
    - Générez votre clef SSH si ce n'est pas déjà fait (`ssh-keygen` - Tapez sur `Entrer` puis `Entrer` puis encore sur `Entrer`)
    - Afficher votre clef `cat ~/.ssh/id_rsa.pub`
    - Copier tout depuis `ssh-rsa AA...` inclus à `= xxxxxxxx@XXXXXXXX` inclus
    - Connectez-vous sur https://bitbucket.org/account/settings/ssh-keys/
    - Cliquez sur `Ajouter une clef`
    - Label: `Ma clé perso`
    - Key : Collez ce que vous avez copié `ssh-rsa AA......= xxxxxxxx@XXXXXXXX`
    - Connectez-vous sur https://bitbucket.org/dupoug_h/wttd/src/master/
    - Cliquez sur `+` en haut à gauche (sous la loupe)
    - Cliquez sur `Fork`
    - Project name: `WttD`
    - Cliquez sur `Clone` en haut à droite (attention l'URL doit être du genre `https://bitbucket.org/monpseudobitbuck/wttd/src/master/` et **NON** `https://bitbucket.org/dupoug_h/wttd/src/master/`)
    - Cliquez sur le bouton copier
    - Coller dans la console WSL le résultat, **NE PAS APPUYER SUR ENTRER**.
    - Ajouter après ce que vous avez collé ceci: `WttD_Contributions`  
    - Appuyez sur `Entrer`
    - Si on vous pose la question `Are you sure you want to continue connecting (yes/no/[fingerprint])?`, tapez `yes` puis entrer
    - Allez dans le dossier GIT: `cd ~/WttD_Contributions`
    - Tapez `git remote add upstream https://bitbucket.org/dupoug_h/wttd/src/master/`

- Procédure (à faire à chaque demande de contribution):
    - Allez dans le dossier GIT: `cd ~/WttD_Contributions`
    - Tapez `git pull upstream master`
    - Si une ligne `Merge branch 'master' of https://bitbucket.org/dupoug_h/wttd/src/master`, appuyez une fois une `Echap` puis tapez `:wq` et enfin `Entrer`
    - Éditer tous les fichers que vous voulez (code, contributions, images, etc.)
    - Ajouter les fichiers modifiés: `git add -A`
    - Expliquez ce qui a été changé: `git commit -m "J'ai vu quelques bugs alors je propose cette correction"`
    - Envoyer la demande: `git push`
    - Connectez-vous sur https://bitbucket.org/monpseudobitbuck/wttd/pull-requests/
    - Cliquez sur `Create pull request`
    - Ajouter un titre indiquand en quelques mots les changements majeurs
    - Ajouter une description (facultative)
    - Cliquez sur `Create`

### Erreurs fréquentes que je vois ###
- Le code écrit en français ... **EN ANGLAIS !!!!!** (vous avez déjà vu du code français ?!?)
- `strcpy` doit auparavant avoir une zone mémoire ALLOUÉE
    - Soit vous passez à `strcpy` un tableau pré-malloc - Ex: `char  name[32]`
    - Soit vous passez à `strcpy` un pointeur déjà alloué - Ex: `char* name = malloc(sizeof(char) * 32);`
    - Quoique vous ayze choisit comme type pour `name`, vous pourrez faire un `strcpy(name, "Hello World!");`
- Vérifiez les fonctions qui n'ont pas été codé par vous
    - Sous Linux, la commande `man` veut dire le **manuel**
    - Dans la console WSL, tapez `man strcpy` ou `man scanf` ou `man strlen` etc.
    - **LISEZ** ces manuels, **RTFM** !! => **R**ead **T**he **F**ucking **M**anual :)
    - Il y a les codes d'erreurs, des exemples, ce que vous ne devez pas faire, etc.
- Les commentaires peuvent se faire ainsi:
```
// Ceci est un commentaire
```
OU
```
return (NULL); // Ceci est un commentaire, mais ne va que de '//' à la fin de la ligne
```
OU
```
/*
Ceci est un bloc de commentaire
et je peux alors écrire sur plusieurs lignes
*/
```
- Comment on écrit des noms de variables ou de fonctions
    - Fichiers   `MyFile.h` and `MyFile.c` , pas de majuscules partout et encore moins à `.h` et `.c`
    - Constantes `#define ALL_UPPERCASE_SEPARATED_BY_UNDERSCORES xxxx`
    - Structures `s_MyStructure` puis avec le typedef `MyStructure`
    - Fonctions  `xxxx myFunctionThatIsDoingThis(xxxx, ...)`
    - Variable   `xxxx my_var_representing_this = xxxx;`
- Les structures se déclarent ainsi:
```
#define MAX_SIZE_FOR_MY_STRING 32

typedef struct s_MyStructureName
{
    int        my_attribute;
    char       my_string[MAX_SIZE_FOR_MY_STRING];
}              MyStructureName;
```
- Un header se définit comme suit:
```
#ifndef MY_FILE_H
#define MY_FILE_H

#include <........h> // Tous les includes systèmes
#include <........h> // sans sauts à la ligne
#include <........h> // et si possible ordonnés alphabétiquement

#include "........h" // Tous vos includes créé par VOUS
#include "........h" // sans sauts à la ligne

#define MY_GLOBAL_CONSTANT   X // Ne pas redéclarer le même nom dans différents fichiers

typedef ...........................; // Structures, etc.

// A comment start of the beginning of the line
// but please align your file as below when possible
// It's much more easier to read, track and find

void    myFunctionPrintingAText         (const char* text);
char    myFunctionNotReturningACharacter();
int     myFunctionToDivideTwoNumber     (int left, int right);
char*   myFunctionToCopyAString         (const char* text, unsigned int text_length);

#endif /* MY_FILE_H */
```
- Un fichier source doit commencer comme suit:
```
#include "MY_FILE_H"

#include <stdlib.h>
#include <unistd.h>

void	myFunctionPrintingAText(const char* text)         // We will NOT edit text so we put a 'const'
{
    unsigned int  text_position = 0;                      // We declare it AND we ALWAYS set a default value 

    if (text == NULL)                                     // Always check is a pointer is not NULL
        printf("Request printing NULL\n");                // Most basic way to print a string, do not forget the '\n'
    else
    {
        while (text[text_position] != '\0')               // While we did not reach the end of string (which is a '\0')
        {
            printf("%c", text[text_position]);            // '%c' for a single character - Do NOT put '\n' here
                                                          // By not putting '\n', printf WILL NOT print yet, he is waiting a '\n'
            text_position++;                              // We go to the next character
        }
        printf("\n");                                     // We can now request printf to print the whole string
    }
}

char	myFunctionNotReturningACharacter()
{
    return ('p');                                         // Returning here the 'p' character, can be anything else
}

int	myFunctionToDivideTwoNumber(int left, int right)
{
    if (right == 0)
    {
        printf("ERROR: Hey! You cannot divide two numbers by 0, you idiot!\n");
        return (0);
    }
    else
        return (left / right);                            // Warning: it will NOT return a floating number!
                                                          // See 'float' or 'double' to get a floating precision
}

char*	          myFunctionToCopyAString(const char* text, unsigned int text_length)
{
    char*         text_copied = NULL;                     // We declare it AND we ALWAYS set a default value 
    unsigned int  text_position = 0;                      // We declare it AND we ALWAYS set a default value 

    if (text == NULL)                                     // Always check is a pointer is not NULL
        return (NULL);                                    // NEVER return a "" when you return a char*
                                                          // Once function is finished, "" will not exist anymore in the memory
                                                          // You can return a basic field such as char, int, void*, char*, etc.
                                                          // but you cannot return a "My Name Is Plop"
                                                          // You MUST allocate memory if you need to return a structure or string
    if ((text_copied = malloc(sizeof(char) * (text_length + 1))) == NULL)    // '+ 1' it is because we need to add the '\0'
        return (NULL);
                                                          // At this point, we have memory allocated for our new string
    while (text_position != text_length)                  // While we did not process all characters in text
    {
        text_copied[text_position] = text[text_position]; // We copy a single character
        text_position++;                                  // We go to the next character 
    }
    text_copied[text_length] = '\0';                      // We indicate here that it is the end of string
    return (text_copied);                                 // We return the string copied
}

```

### À faire ###
- Blâmer le professeur pour écrire ses codes d'exemple en français
- Blâmer le professeur pour demander aux étudiants de faire le projet sur CodeBlocks
- Blâmer le professeur pour demander aux étudiants d'utiliser des fonctions deprecated (`scanf`, `gets`, etc.)
- Blâmer le professeur pour ne pas avoir installé un GIT local sur l'infrastructure de son école