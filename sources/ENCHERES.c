#include <stdio.h>
#include <stdlib.h>

#include "ENCHERES.h"
#include "MONSTRE.h"
#include "DONJON.h"
#include "AVENTURIER.h"
#include "PIOCHE_MONSTRE.h"
#include "MISE_EN_PLACE.h"

void                lance_les_encheres(Joueur* joueur_qui_joue, Donjon* donjon, PiocheMonstre* pioche_monstre, Aventurier* aventurier)
{
    unsigned int    choix_action = 0;

    printf(" ___________  Que voulez-vous faire ?  ___________________________________________ \n");
    printf("|                                                                                 |\n");
    printf("|   1. Passez votre tour                                                          |\n");
    printf("|   2. Piochez une carte                                                          |\n");
    printf("|_________________________________________________________________________________|\n");
    choix_action = lire_un_chiffre_depuis_la_console("Choisissez votre action pour la phase d'ench�res : ", "Le choix n'est pas valide\n", 1, 2);
    printf("\n");
    switch (choix_action)
    {
        case 1 : action_de_passer_le_tour(joueur_qui_joue);
            break;
        case 2 : action_de_pioche_une_carte(joueur_qui_joue, donjon, pioche_monstre, aventurier);
            break;
        default : printf("ERREUR : laissez ce pauvre programme tranquille svp\n");
    }
}

void                action_de_passer_le_tour(Joueur* joueur_qui_joue)
{
  joueur_qui_joue->passer_le_tour = JOUEUR_A_PASSER;
}

void                action_de_pioche_une_carte(Joueur* joueur_qui_joue, Donjon* donjon, PiocheMonstre* pioche_monstre, Aventurier* aventurier)
{
    unsigned int    choix_action_apres_avoir_pioche = 0;

    joueur_tire_une_carte_monstre(joueur_qui_joue, pioche_monstre);
    printf(" ___________  Vous avez pioch� une carte, que souhaitez-vous en faire ?  _________ \n");
    printf("|                                                                                 |\n");
    printf("|   1. Mettre le monstre dans le donjon                                           |\n");
    printf("|   2. Retirez le monstre ainsi qu'un �quipement                                  |\n");
    printf("|_________________________________________________________________________________|\n");
    choix_action_apres_avoir_pioche = lire_un_chiffre_depuis_la_console("Choisissez votre action apr�s avoir pioch� : ", "Le choix n'est pas valide\n", 1, 2);
    printf("\n");
    switch (choix_action_apres_avoir_pioche)
    {
        case 1 : mettre_carte_dans_le_donjon(joueur_qui_joue, donjon);
            break;
        case 2 : retirez_monstre_et_equipement(joueur_qui_joue, aventurier);
            break;
        default : printf("Faut choisir entre 1 et 2 c'est pourtant pas compliqu�, m�me un STMG sait le faire :-)\n");
    }
}

void                mettre_carte_dans_le_donjon(Joueur* joueur_qui_joue, Donjon* donjon)
{
   ajouter_un_monstre_au_donjon(donjon, joueur_qui_joue->monstre_dans_ma_main);
   if (joueur_qui_joue->monstre_dans_ma_main == NULL)
        printf("ca craint\n");
   printf("Vous avez mis le %s dans le donjon\n\n", joueur_qui_joue->monstre_dans_ma_main->nom);
}

void                retirez_monstre_et_equipement(Joueur* joueur_qui_joue, Aventurier* aventurier)
{
    unsigned int    choix_equipement_a_enlever = 0;

    afficher_les_equipements_de_aventurier (aventurier);
    choix_equipement_a_enlever = lire_un_chiffre_depuis_la_console("Choisissez l'�quipement � retirer : ", "Le choix n'est pas valide\n", 1, 6);
    printf("\n");
    retirer_un_equipement_de_aventurier(aventurier, choix_equipement_a_enlever - 1);
    printf("\n");
   (void)joueur_qui_joue;
}
